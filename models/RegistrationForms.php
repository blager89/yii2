<?php

namespace app\models;
use yii\base\Model;



class RegistrationForms extends  Model
{
    public $email;
    public $password;
    public $name;
    public $country;
    public $sex;


    public function attributeLabels()
    {
            return [
                'email' => 'Емейл',
                'password' => 'Пароль',
                'name' => 'Ім\'я',
                'country' => 'Країна',
                'sex' => 'Стать'
            ];
    }
    public function rules()
    {
        return [
            [['email','password'],'required','message' => 'Поле обов\'язкове'],
            ['email','email','message' => 'Некоректний Email'],
            ['password', 'string','min'=>3, 'tooShort' => 'Значення «Пароль» має містити мінімум 5 символів']
        ];
    }

}