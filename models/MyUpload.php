<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class MyUpload extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'maxFiles' => 10],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $file->saveAs('files/' . $file->baseName . '.' . $file->extension);

            }
            return true;
        } else {
            return false;
        }
    }

    public function download()
    {

        return $files=\yii\helpers\FileHelper::findFiles('files');
    }
}
