<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>



<?php $form = ActiveForm::begin(['options' => ['id' => 'regForm']]); ?>
<?= $form->field($model, 'email')->input('email')?>
<?= $form->field($model, 'password')->passwordInput()?>
<?= $form->field($model, 'name')?>
<?= $form->field($model, 'country')?>
<?/*= $form->field($model, 'sex')->label('Стать')*/?>
<?= $form->field($model, 'sex')->dropDownList(['1' => 'Чоловік', '0' => 'Жінка'],['prompt'=>'Вибір статі']);?>
<?= Html::submitButton('Відправити', ['class'=> 'btn btn-success'])?>
<? $form = ActiveForm::end(); ?>