<?php
use yii\widgets\ActiveForm;
?>

<?php $upload = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $upload->field($model, 'imageFiles[]')->fileInput(['multiple' => true])->label('Завантажити файл') ?>

    <button>Submit</button>

<?php ActiveForm::end() ?>