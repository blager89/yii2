<?php

namespace app\controllers;
use yii\web\Controller;
use app\models\RegistrationForms;


class MyController extends Controller
{
    public function actionRegistration()
    {
        $model = new RegistrationForms();
        return $this->render('registration',compact('model'));
    }
}